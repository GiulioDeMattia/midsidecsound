<CsoundSynthesizer>
<CsOptions>
-o "pinuccciodisatteso.wav" -W
</CsOptions>
<CsInstruments>
/*
 *  Copyright (c) 2024 Giulio Romano De Mattia
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
;-------------------------------------------------------------------------------------


sr = 96000
ksmps = 32
nchnls = 2
0dbfs = 1
#include "./Macro/globVar.lib"
#include "./Udo"


opcode MS_ORDER_II,kkkkkkk,kk
	ka,ke xin
	k0 = 1
	k1 = sin(ka)*cos(ke)
	k3 = cos(ka)*cos(ke)
	// second order
	k4 = sqrt(3/4)*sin(2*ka)*(cos(ke))^2 
	k5 = sqrt(3/4)*sin(ka)*sin(2*ke)
	k7 = sqrt(3/4)*cos(ka)*sin(2*ke)
	k8 = sqrt(3/4)*cos(2*ka)*(cos(ke))^2
	xout k0,k1,k3,k4,k5,k7,k8
endop



opcode	mTO_ORDER_II, aaaaaaa,akk
	amono,ka,ke xin
	k0,k1,k3,k4,k5,k7,k8 MS_ORDER_II ka,ke
	a0 = amono * k0
	a1 = amono * k1
	a3 = amono * k3
	a4 = amono * k4
	a5 = amono * k5
	a7 = amono * k7
	a8 = amono * k8
	xout a0,a1,a3,a4,a5,a7,a8
endop



opcode VMIC_II, a, aaaaaaakkk
	a0,a1,a3,a4,a5,a7,a8,ka,ke,k_pp xin
	k0,k1,k3,k4,k5,k7,k8 MS_ORDER_II ka,ke
	avmic = a0*k0*(1-k_pp) + (k_pp * (a1*k1+a3*k3+a4*k4+a5*k5+a7*k7+a8*k8))/2
	xout	avmic
endop




instr 1
	istart=p4
	iaf=p5
	kein=p6
	kpp=p7
	kamove line istart,p3,iaf
	ka = kamove*$M_PI/180
	ke = kein*$M_PI/180
	
	as oscil 1,440,2
	a0,a1,a3,a4,a5,a7,a8 mTO_ORDER_II as, ka,ke
	k1a=$M_PI_4
	k1e = 0
	aL VMIC_II a0,a1,a3,a4,a5,a7,a8,k1a,k1e,kpp
	aR VMIC_II a0,a1,a3,a4,a5,a7,a8,-k1a,k1e,kpp

;outs a0,a1,a3,a4,a5,a7,a8
outs	aL,aR
endin

</CsInstruments>
<CsScore>
f2	0	4096	10	1	1	1	1	1	1	1	1	1	1	1	1
i1		0		7		0		135		0			1



</CsScore>
</CsoundSynthesizer>


<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="background">
  <r>240</r>
  <g>240</g>
  <b>240</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
