# Custom Library Documentation

## MS_ORDER_II Opcode

### Description
 The `MS_ORDER_II` opcode calculates a set of variables based on the input parameters `ka` and `ke`.
 These variables represent a second-order mathematical transformation.

### Parameters
 Input parameters ka and ke
- `ka`: Input parameter representing a value.
- `ke`: Input parameter representing a value.

### Output Variables
 Output variables k0 to k8
1. `k0`: Initialized to 1.
2. `k1`: Calculated as `sin(ka) * cos(ke)`.
3. `k3`: Calculated as `cos(ka) * cos(ke)`.
4. `k4`: Calculated as `sqrt(3/4) * sin(2*ka) * (cos(ke))^2`.
5. `k5`: Calculated as `sqrt(3/4) * sin(ka) * sin(2*ke)`.
6. `k7`: Calculated as `sqrt(3/4) * cos(ka) * sin(2*ke)`.
7. `k8`: Calculated as `sqrt(3/4) * cos(2*ka) * (cos(ke))^2`.

### Usage
```csound
opcode MS_ORDER_II, kkkkkkk, kk
    ka, ke xin

    # Initialize k0 to 1
    k0 = 1

    # Calculate k1 using sine and cosine functions
    k1 = sin(ka) * cos(ke)

    # Calculate k3 using cosine functions
    k3 = cos(ka) * cos(ke)

    # Calculate k4 using sine and cosine functions
    k4 = sqrt(3/4) * sin(2*ka) * (cos(ke))^2 

    # Calculate k5 using sine and cosine functions
    k5 = sqrt(3/4) * sin(ka) * sin(2*ke)

    # Calculate k7 using sine and cosine functions
    k7 = sqrt(3/4) * cos(ka) * sin(2*ke)

    # Calculate k8 using sine and cosine functions
    k8 = sqrt(3/4) * cos(2*ka) * (cos(ke))^2

    # Output the variables k0, k1, k3, k4, k5, k7, k8
    xout k0, k1, k3, k4, k5, k7, k8
endop
```

## mTO_ORDER_II Opcode

### Description

The mTO_ORDER_II opcode extends the functionality of MS_ORDER_II by introducing an additional input parameter amono.
It calculates a set of variables by multiplying the output of MS_ORDER_II with the amono value.

Parameters

Input parameters amono, ka, and ke
amono: Input parameter representing a value.
ka: Input parameter representing a value.
ke: Input parameter representing a value.
Output Variables
Output variables a0 to a8
a0: Calculated as amono * k0.
a1: Calculated as amono * k1.
a3: Calculated as amono * k3.
a4: Calculated as amono * k4.
a5: Calculated as amono * k5.
a7: Calculated as amono * k7.
a8: Calculated as amono * k8.

### Usage

```csound


opcode mTO_ORDER_II, aaaaaaa, akk
    amono, ka, ke xin

    # Calculate k0 to k8 using the MS_ORDER_II opcode with ka and ke
    k0, k1, k3, k4, k5, k7, k8 MS_ORDER_II ka, ke

    # Calculate output variables using amono and the calculated k values
    a0 = amono * k0
    a1 = amono * k1
    a3 = amono * k3
    a4 = amono * k4
    a5 = amono * k5
    a7 = amono * k7
    a8 = amono * k8

    # Output the variables a0, a1, a3, a4, a5, a7, a8
    xout a0, a1, a3, a4, a5, a7, a8
endop
```

## VMIC_II Opcode

### Description

The VMIC_II opcode utilizes the outputs of MS_ORDER_II to calculate a variable avmic based on input vectors and parameters.

### Parameters

Input parameters and vectors
a0, a1, a3, a4, a5, a7, a8: Input vector representing values.
ka, ke: Input parameters representing values.
k_pp: Input parameter representing a value.
Output Variable
Output variable avmic
avmic: Calculated as a combination of input values using the MS_ORDER_II outputs.

### Usage

```csound 
opcode VMIC_II, a, aaaaaaakkk
    # Input parameters and vectors
    a0, a1, a3, a4, a5, a7, a8, ka, ke, k_pp xin

    # Calculate k values using the MS_ORDER_II opcode with ka and ke
    k0, k1, k3, k4, k5, k7, k8 MS_ORDER_II ka, ke

    # Calculate avmic using input vectors, k values, and k_pp
    avmic = a0 * k0 * (1 - k_pp) + (k_pp * (a1 * k1 + a3 * k3 + a4 * k4 + a5 * k5 + a7 * k7 + a8 * k8)) / 2

    # Output the variable avmic
    xout avmic
endop
```



